package test;

import domain.Empleado;
import domain.Gerente;

public class TestIntanceOf {
    public static void main(String[] args) {
        Empleado empleado = new Empleado("Juan", 5000);
        Empleado empleado2 = new Gerente("Karla", 10000, "Contabilidad");
        determinarTipo(empleado);
        determinarTipo(empleado2);
    }

    public static void determinarTipo(Empleado empleado) {
        System.out.print("El empleado: ");
        if (empleado instanceof Gerente) {
            System.out.println("Es de tipo Gerente");
            Gerente gerente = (Gerente) empleado;
            System.out.println("Gerente del departamento " + gerente.getDepartamento());
        }
        else if(empleado instanceof Empleado){
            System.out.println("Es de tipo Empleado");
        }
        else if(empleado instanceof Object){
            System.out.println("Es de tipo Object");
        }
    }
}
