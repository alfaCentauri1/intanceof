package domain;

public class Empleado {
    private String name;
    private float sueldo;

    public Empleado(String name, float sueldo) {
        this.name = name;
        this.sueldo = sueldo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getSueldo() {
        return sueldo;
    }

    public void setSueldo(float sueldo) {
        this.sueldo = sueldo;
    }
    public String obtenerDetalles(){
        return "Nombre: " + this.name + ", sueldo: " + this.sueldo;
    }
}
